package it.unibas.mediapesataswing.modello;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class StudenteTest {

	
	@Test
	public void studenteShouldReturnNome() {
		// Arrange
		Studente studente = new Studente();
		String nome = "Mario";
		
		// Act
		studente.setNome(nome);
		String actual = studente.getNome();
		
		// Assert
		assertEquals(nome, actual);
	}
	
	@Test
	public void studenteShouldReturnCognome() {
		// Arrange
		Studente studente = new Studente();
		String cognome = "Rossi";
		
		// Act
		studente.setCognome(cognome);
		String actual = studente.getCognome();
		
		// Assert
		assertEquals(cognome, actual);
	}
	
	@Test
	public void studenteShouldReturnMatricola() {
		// Arrange
		Studente studente = new Studente();
		int matricola = 1234;
		
		// Act
		studente.setMatricola(matricola);
		int actual = studente.getMatricola();
		
		// Assert
		assertEquals(matricola, actual);
	}
	
	@Test
	public void studenteShouldReturnEsame() {
		// Arrange
		Studente studente = new Studente();
		Esame esame = new Esame("", 18, false, 1);
		
		// Act
		studente.addEsame(esame);
		Esame actual = studente.getEsame(0);
		
		// Assert
		assertEquals(esame, actual);
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void getEsameShouldRaiseException() {
		// Arrange
		Studente studente = new Studente();
		
		// Act
		studente.getEsame(0);
	}
	
	@Test
	public void studenteShouldReturnNumeroEsami() {
		// Arrange
		Studente studente = new Studente();
		int numeroEsami = 2;
		
		// Act
		studente.addEsame("a", 18, false, 1);
		studente.addEsame("b", 19, false, 2);
		studente.addEsame("c", 20, false, 3);
		studente.eliminaEsame(2);
		int actual = studente.getNumeroEsami();
		
		// Assert
		assertEquals(numeroEsami, actual);
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void eliminaEsameShouldRaiseException() {
		// Arrange
		Studente studente = new Studente();
		
		// Act
		studente.eliminaEsame(0);
	}
	
	@Test
	public void studenteShouldReturnListaEsami() {
		// Arrange
		Esame esame1 = new Esame("a", 18, false, 1);
		Esame esame2 = new Esame("b", 19, false, 2);
		List<Esame> listaEsami = new ArrayList<>();
		listaEsami.add(esame1);
		listaEsami.add(esame2);
		Studente studente = new Studente();
		
		// Act
		studente.addEsame(esame1);
		studente.addEsame(esame2);
		var actual = studente.getListaEsami();
		
		// Assert
		assertTrue(listaEsami.equals(actual));
	}
	
	@Test
	public void studenteShouldReturnMediaPesata() {
		// Assert
		Studente studente = new Studente();
		Esame esame1 = new Esame("a", 18, false, 2);
		Esame esame2 = new Esame("b", 19, false, 3);
		double mediaPesata = 18.600;
		
		// Act
		studente.addEsame(esame1);
		studente.addEsame(esame2);
		double actual = studente.getMediaPesata();
		
		// Arrange
		assertEquals(mediaPesata, actual, 0.001);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void getMediaPesataShouldRaiseException() {
		// Arrange
		Studente studente = new Studente();
		
		// Act
		studente.getMediaPesata();
	}
	
	@Test
	public void studenteShouldReturnMediaPartenza() {
		// Arrange
		Studente studente = new Studente();
		Esame esame1 = new Esame("a", 18, false, 2);
		Esame esame2 = new Esame("b", 19, false, 3);
		double mediaPartenza = 68.200;
		
		// Act
		studente.addEsame(esame1);
		studente.addEsame(esame2);
		double actual = studente.getMediaPartenza();
		
		// Assert
		assertEquals(mediaPartenza, actual, 0.001);
	}
	
	@Test
	public void studenteShouldReturnStringFromToString() {
		// Arrange
		String nome = "Mario";
		String cognome = "Rossi";
		int matricola = 1234;
		Studente studente = new Studente(nome, cognome, matricola);
		String toString = "Cognome: " + cognome 
						+ " - Nome: " + nome
						+ " - Matricola: " + matricola;
		
		// Act
		String actual = studente.toString();
		
		// Assert
		assertEquals(toString, actual);
	}
	
	@Test
	public void studenteShouldReturnStringFromToSaveString() {
		// Arrange
		String nome = "Mario";
		String cognome = "Rossi";
		int matricola = 1234;
		Studente studente = new Studente(nome, cognome, matricola);
		String toSaveString = cognome + " , "
							+ nome + " , " 
							+ matricola;
		
		// Act
		String actual = studente.toSaveString();
		
		// Assert
		assertEquals(toSaveString, actual);
	}

}
