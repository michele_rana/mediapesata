package it.unibas.mediapesataswing.modello;

import static org.junit.Assert.*;

import org.junit.Test;

public class EsameTest {

	@Test
	public void esameShouldReturnInsegnamento() {
		// Arrange
		Esame esame = new Esame("", 18, false, 1);
		String insegnamento = "Integrazione e test di sistemi software";
		
		// Act
		esame.setInsegnamento(insegnamento);
		String actual = esame.getInsegnamento();
		
		// Assert
		assertEquals(insegnamento, actual);
	}
	
	@Test
	public void esameShouldReturnVoto() {
		// Arrange
		Esame esame = new Esame("", 18, false, 1);
		int voto = 21;
		
		// Act
		esame.setVoto(voto);
		int actual = esame.getVoto();
		
		// Assert
		assertEquals(voto, actual);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setVotoShouldRaiseException() {
		// Arrange
		Esame esame = new Esame("", 18, false, 1);
		int voto = 17;
		
		// Act
		esame.setVoto(voto);
	}
	
	@Test
	public void esameShouldReturnLode() {
		// Arrange
		Esame esame = new Esame("", 18, false, 1);
		int voto = 30;
		boolean lode = true;
		esame.setVoto(voto);
		
		// Act
		esame.setLode(lode);
		boolean actual = esame.isLode();
		
		// Assert
		assertEquals(lode, actual);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setLodeShouldRaiseException() {
		// Arrange
		Esame esame = new Esame("", 18, false, 1);
		boolean lode = true;
		
		// Act
		esame.setLode(lode);
	}
	
	@Test
	public void esameShouldReturnCrediti() {
		// Arrange
		Esame esame = new Esame("", 18, false, 1);
		int crediti = 9;
		
		// Act
		esame.setCrediti(crediti);
		int actual = esame.getCrediti();
		
		// Assert
		assertEquals(crediti, actual);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setCreditiShouldRaiseException() {
		// Arrange
		Esame esame = new Esame("", 18, false, 1);
		int crediti = 0;
		
		// Act
		esame.setCrediti(crediti);
	}
	
	@Test
	public void esameShouldReturnStringFromToStringWithoutLode() {
		// Arrange
		String insegnamento = "Integrazione e test di sistemi software";
		int voto = 30;
		boolean lode = false;
		int crediti = 9;
		Esame esame = new Esame(insegnamento, voto, lode, crediti);
		String toString = "Esame di " + insegnamento 
		                + " (" + crediti + " CFU) - voto: " + voto;
		
		// Act
		String actual = esame.toString();
		
		// Assert
		assertEquals(toString, actual);
	}
	
	@Test
	public void esameShouldReturnStringFromToStringWithLode() {
		// Arrange
		String insegnamento = "Integrazione e test di sistemi software";
		int voto = 30;
		boolean lode = true;
		int crediti = 9;
		Esame esame = new Esame(insegnamento, voto, lode, crediti);
		String toString = "Esame di " + insegnamento 
		                + " (" + crediti + " CFU) - voto: " + voto + " e lode"; 
		
		// Act
		String actual = esame.toString();
		
		// Assert
		assertEquals(toString, actual);
	}
	
	@Test
	public void esameShouldReturnStringFromToSaveString() {
		// Arrange
		String insegnamento = "Integrazione e test di sistemi software";
		int voto = 30;
		boolean lode = true;
		int crediti = 9;
		Esame esame = new Esame(insegnamento, voto, lode, crediti);
		String toSaveString = insegnamento + " , " + crediti 
						+ " , " + voto + " , " + lode;
		
		// Act
		String actual = esame.toSaveString();
		
		// Assert
		assertEquals(toSaveString, actual);
	}
	
}
