package it.unibas.mediapesataswing.vista;

import it.unibas.mediapesataswing.Costanti;
import it.unibas.mediapesataswing.controllo.Controllo;
import it.unibas.mediapesataswing.modello.Esame;
import it.unibas.mediapesataswing.modello.Modello;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class FinestraEsame extends JDialog {
    
    private Vista vista;
    private Controllo controllo;
    private Modello modello;
    
    private JTextField campoCrediti = new JTextField();
    private JTextField campoInsegnamento = new JTextField();
    private JTextField campoVoto = new JTextField();
    private JCheckBox checkBoxLode = new JCheckBox();
    
    public String getCampoInsegnamento() {
        return this.campoInsegnamento.getText();
    }
    
    public String getCampoCrediti() {
        return this.campoCrediti.getText();
    }
    
    public String getCampoVoto() {
        return this.campoVoto.getText();
    }
    
    public boolean getCheckLode() {
        return this.checkBoxLode.isSelected();
    }

    public FinestraEsame(Vista vista) {
        super(vista);
        this.vista = vista;
        this.controllo = vista.getControllo();
        this.modello = vista.getModello();
        this.setModal(true);
        this.inizializza();
    }
        
    public void inizializza() {
        JPanel pannelloEsame = new JPanel();
        JLabel labelInsegnamento = new JLabel("Insegnamento");
        campoInsegnamento.setColumns(20);
        pannelloEsame.add(labelInsegnamento);
        pannelloEsame.add(campoInsegnamento);
        JLabel labelCrediti = new JLabel("Crediti");
        campoCrediti.setColumns(3);
        pannelloEsame.add(labelCrediti);
        pannelloEsame.add(campoCrediti);
        JLabel labelVoto = new JLabel("Voto");
        campoVoto.setColumns(2);
        pannelloEsame.add(labelVoto);
        pannelloEsame.add(campoVoto);
        JLabel labelLode = new JLabel("Lode");
        pannelloEsame.add(labelLode);
        pannelloEsame.add(checkBoxLode);
        Action azioneAggiornaEsame = this.controllo.getAzione(Costanti.AZIONE_AGGIORNA_ESAME);
        JButton bottoneAggiornaEsame = new JButton(azioneAggiornaEsame);
        campoInsegnamento.setAction(azioneAggiornaEsame);
        campoCrediti.setAction(azioneAggiornaEsame);
        campoVoto.setAction(azioneAggiornaEsame);
        pannelloEsame.add(bottoneAggiornaEsame);
        this.getContentPane().add(pannelloEsame);
        this.setTitle("Modifica i Dati dell'Esame");
        this.pack();        
    }
    
    public void visualizza() {
        this.schermoEsame();
        this.setVisible(true);
    }
        
    public void nascondi() {
        this.setVisible(false);
    }

    public void schermoEsame() {
        Modello modello = this.controllo.getModello();
        Esame esame = (Esame)modello.getBean(Costanti.ESAME);
        this.campoInsegnamento.setText(esame.getInsegnamento());
        this.campoCrediti.setText(esame.getCrediti() + "");
        this.campoVoto.setText(esame.getVoto() + "");
        this.checkBoxLode.setSelected(esame.isLode());
    }
    
    public void disabilitaControlli() {
        this.campoCrediti.setEnabled(false);
        this.campoInsegnamento.setEnabled(false);
        this.campoVoto.setEnabled(false);
        this.checkBoxLode.setEnabled(false);
    }

    public void abilitaControlli() {
        this.campoCrediti.setEnabled(true);
        this.campoInsegnamento.setEnabled(true);
        this.campoVoto.setEnabled(true);
        this.checkBoxLode.setEnabled(true);
    }
    
}
