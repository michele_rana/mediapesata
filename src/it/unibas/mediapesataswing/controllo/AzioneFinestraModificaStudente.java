package it.unibas.mediapesataswing.controllo;

public class AzioneFinestraModificaStudente extends javax.swing.AbstractAction {
    
    private Controllo controllo;
    
    public AzioneFinestraModificaStudente(Controllo controllo) {
        this.controllo = controllo;
        this.putValue(javax.swing.Action.NAME, "Modifica Studente");
        this.putValue(javax.swing.Action.SHORT_DESCRIPTION, "Modifica i dati dello studente");
        this.putValue(javax.swing.Action.MNEMONIC_KEY, new Integer(java.awt.event.KeyEvent.VK_M));
    }
    
    public void actionPerformed(java.awt.event.ActionEvent e) {
        this.controllo.getVista().finestraModificaStudente();
    }
    
}
