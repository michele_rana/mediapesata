package it.unibas.mediapesataswing.controllo;

import it.unibas.mediapesataswing.Costanti;
import it.unibas.mediapesataswing.modello.Modello;
import it.unibas.mediapesataswing.modello.Studente;
import it.unibas.mediapesataswing.vista.FinestraStudente;
import it.unibas.mediapesataswing.vista.PannelloPrincipale;
import it.unibas.mediapesataswing.vista.Vista;

public class AzioneNuovo extends javax.swing.AbstractAction {
    
    private Controllo controllo;
    
    public AzioneNuovo(Controllo controllo) {
        this.controllo = controllo;
        this.putValue(javax.swing.Action.NAME, "Nuovo Studente");
        this.putValue(javax.swing.Action.SHORT_DESCRIPTION, "Crea un nuovo studente");
        this.putValue(javax.swing.Action.MNEMONIC_KEY, new Integer(java.awt.event.KeyEvent.VK_N));
        this.putValue(javax.swing.Action.ACCELERATOR_KEY, javax.swing.KeyStroke.getKeyStroke("ctrl N"));
    }
    
    public void actionPerformed(java.awt.event.ActionEvent e) {
        Modello modello = this.controllo.getModello();
        modello.putBean(Costanti.STUDENTE, new Studente());
        abilitaAzioni();
        Vista vista = controllo.getVista();
        PannelloPrincipale pannello = (PannelloPrincipale)vista.getSottoVista(Costanti.VISTA_PANNELLO_PRINCIPALE);
        pannello.schermoNuovoStudente();
        FinestraStudente finestraStudente = (FinestraStudente)vista.getSottoVista(Costanti.VISTA_FINESTRA_STUDENTE);
        finestraStudente.visualizza();
    }
    
    private void abilitaAzioni() {
        this.controllo.getAzione(Costanti.AZIONE_INSERISCI_ESAME).setEnabled(true);
        this.controllo.getAzione(Costanti.AZIONE_MODIFICA_ESAME).setEnabled(true);
        this.controllo.getAzione(Costanti.AZIONE_ELIMINA_ESAME).setEnabled(true);
        this.controllo.getAzione(Costanti.AZIONE_MODIFICA_STUDENTE).setEnabled(true);
        this.controllo.getAzione(Costanti.AZIONE_CALCOLA_MEDIA).setEnabled(true);
        this.controllo.getAzione(Costanti.AZIONE_SALVA).setEnabled(true);        
        this.controllo.getAzione(Costanti.AZIONE_VISUALIZZA_TABELLA).setEnabled(true);        
        this.controllo.getAzione(Costanti.AZIONE_APRI_EXCEL).setEnabled(true);
        this.controllo.getAzione(Costanti.AZIONE_APRI_EXCEL_NO_THREAD).setEnabled(true);        
    }
}
