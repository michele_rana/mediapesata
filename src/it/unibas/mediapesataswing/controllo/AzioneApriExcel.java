package it.unibas.mediapesataswing.controllo;

import it.unibas.mediapesataswing.Costanti;
import it.unibas.mediapesataswing.modello.Modello;
import it.unibas.mediapesataswing.modello.Studente;
import it.unibas.mediapesataswing.persistenza.DAOException;
import it.unibas.mediapesataswing.persistenza.DAOStudente;
import it.unibas.mediapesataswing.vista.Vista;
import java.io.IOException;
import javax.swing.AbstractAction;

public class AzioneApriExcel extends AbstractAction {

    private Controllo controllo;

    public AzioneApriExcel(Controllo controllo) {
        this.controllo = controllo;
        this.putValue(javax.swing.Action.NAME, "Apri con Excel");
        this.putValue(javax.swing.Action.SHORT_DESCRIPTION, "Apre il documento con Microsoft Excel");
        this.putValue(javax.swing.Action.MNEMONIC_KEY, new Integer(java.awt.event.KeyEvent.VK_L));
    }

    public void actionPerformed(java.awt.event.ActionEvent e) {
        String tmpDir = System.getProperty("java.io.tmpdir");
        String nomeFile = tmpDir + "studenteTmp.csv";
        Vista vista = this.controllo.getVista();
        Modello modello = this.controllo.getModello();
        Studente studente = (Studente) modello.getBean(Costanti.STUDENTE);
        if (studente == null) {
            vista.finestraErrore("Lo studente � nullo");
            return;
        }
        try {
            DAOStudente.salva(studente, nomeFile);
            eseguiProcesso(nomeFile);
        } catch (DAOException daoe) {
            vista.finestraErrore("Impossibile salvare il file temporaneo: " + daoe);
        }
    }

    private void eseguiProcesso(final String nomeFile) {
        final Vista vista = this.controllo.getVista();
        Modello modello = this.controllo.getModello();
        Studente studente = (Studente) modello.getBean(Costanti.STUDENTE);
        final javax.swing.SwingWorker worker = new javax.swing.SwingWorker() {

            int exitCode = -1;

            protected Object doInBackground() throws Exception {
                try {
                    Runtime runtime = Runtime.getRuntime();
                    Process process = runtime.exec(Costanti.ESEGUIBILE_EXCEL + " " + nomeFile);
                    process.waitFor();
                    this.exitCode = process.exitValue();
                } catch (InterruptedException e) { // per waitFor()
                } catch (IOException ioe) {
                    vista.finestraErrore("Impossibile eseguire Microsoft Excel: " + ioe);
                }
                return null;
            }

            protected void done() {
                try {
                    if (exitCode != 0) {
                        vista.finestraErrore("Errori nell'esecuzione del processo");
                    } else {
                        vista.toFront();
                    }
                } catch (Exception ignore) {
                }
            }
        };

        worker.execute();
    }
}
